== README

This task consist on fetching data from Libring api.
Insert those data on database(postgres). After that run a app that fetches those data from the database and display them on a 	datatable.
	

screenshot url:
https://drive.google.com/file/d/1rENMPZ5dgnGbeW52dEzh3APOw8RZxyw-/view?usp=sharing


* Ruby version
	Rails 4.2.6
	ruby 2.2.0

* Database initialization
	This was the connection parameters of my pc it will need to be modified on other pc.

	1-> To run the script (FILE: script.rb) 
	  PG.connect :dbname => 'jobTask2', :user => 'postgres', :password => 'estrelas', :host => 'localhost'
	2->app (FILE: database.yml)
	  database: jobTask2
	  username: postgres
	  password: estrelas


* Deployment instructions
	1-> Go inside app folder and type "rake db:migrate" it will create the table in the database.
	2-> Go to root folder and run "ruby script.rb <start-date> <end-date>" eg: "ruby script.rb "2018-02-10" "2018-02-10", this script will insert data inside the table.
	3-> Go inside app folder and run the app with the command "rails s"
	4-> Acess screen using the following link http://localhost:3000/logs
* ...
