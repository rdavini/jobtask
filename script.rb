require 'net/https'
require 'uri'
require 'json'
require 'pg'


#get input parameters
dataIni = ARGV[0]
dataFim = ARGV[1]

@requestStr= "https://api.libring.com/v2/reporting/get?start_date= #{dataIni}&end_date=#{dataFim}&period=custom_date&group_by=date,connection,app,platform,country&data_type=adnetwork"
loop do 
	#fetch API DATA																																																																																		
	uri = URI.parse(@requestStr)
	https = Net::HTTP.new(uri.host, uri.port)
	https.use_ssl = true
	req = Net::HTTP::Get.new(uri.request_uri, initheader = {'Content-Type' =>'application/json', 'Authorization' => 'Token HYCRvSlRAEyRrsHbujvlaoRPf'})
	resp = https.request(req)
	data = JSON.parse(resp.body)

	#loop throught data and insert each row in dataBase
	con = PG.connect :dbname => 'jobTask2', :user => 'postgres', :password => 'estrelas', :host => 'localhost'
	data['connections'].each do |child|
	    con.exec("INSERT INTO logs(app, connection, platform, country, impressions, ad_revenue, created_at) values ('#{child['app']}', '#{child['connection']}', '#{child['platform']}', '#{child['country']}', '#{child['impressions']}', '#{child['ad_revenue']}', '#{child['date']}')")
	end

  # some code here
  break if data['next_page_url'].empty?

  @requestStr= data['next_page_url']
end 

