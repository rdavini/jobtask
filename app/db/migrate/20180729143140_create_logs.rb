class CreateLogs < ActiveRecord::Migration
  def change
    create_table :logs do |t|
      t.string :app
      t.string :connection
      t.string :platform
      t.string :country
      t.integer :impressions
      t.float :ad_revenue

      t.timestamps
    end
  end
end
