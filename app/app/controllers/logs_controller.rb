class LogsController < ApplicationController
	def index
		@logs = Log.all

		@dataInterval = Log.select("min(created_at) AS min_date, max(created_at) AS max_date")

		@min_date= @dataInterval[0].min_date
		@max_date= @dataInterval[0].max_date

		render 'index', :locals => {:logs => @logs, :min_date => @min_date, :max_date => @max_date}
	end

	def ajaxRequest
		@startDate = Date.parse(params['startDate'])
		@endDate = Date.parse(params['endDate'])

		@logs = Log.select("app, connection, country, platform, date(created_at), impressions, ad_revenue, trunc((ad_revenue / nullif((impressions/1000), 0))::numeric, 2) as cpm").where(created_at: @startDate..@endDate)
		
		
		render 'index', :json => {:data => @logs}
	end

	def ajaxGroupByRequest
		@inputs = [
			params["app"],
			params["connection"],
			params["country"],
			params["platform"],
			params["date(created_at)"]
		]

		#build query
		@sqlInputs= ""
		@size = @inputs.length-1
		for i in 0..@size
			if(!@inputs[i].blank?)
				if(!@sqlInputs.blank?)
					@sqlInputs << ','
				end

				@sqlInputs << @inputs[i]
			end
		end
		

		@logs = Log.select("#{@sqlInputs}, sum(impressions) as impressions, sum(ad_revenue) as ad_revenue, trunc((sum(ad_revenue)/(nullif((sum(impressions) /1000), 0)))::numeric, 2) as cpm").group("#{@sqlInputs}")

		render 'index', :json => {:data => @logs}
	end
end